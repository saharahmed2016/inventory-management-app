﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class Stock
    {
        public string Name { get; set; }
        public int? SellinDays { get; set; }
        public int? Quality { get; set; }
    }
}
