﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public static class ItemNames
    {
        public const string AgedBrie = "aged brie";
        public const string BackstagePasses = "backstage passes";
        public const string Sulfuras = "sulfuras";
        public const string NormalItem = "normal item";
        public const string Conjured = "conjured";
    }

}
