﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Configuration;

namespace SharedDll
{
    public class LogWriter
    {

        private static string LogDirectory = String.Empty;
        public bool WriteErrorLog(string logMessage)
        {
            bool successStatus = false;

            LogDirectory = ConfigurationManager.AppSettings["LogDirectory"].ToString();
            DateTime currentDateTime = DateTime.Now;
            string currentDateTimeString = currentDateTime.ToString();
            CheckCreateLogDirectory(LogDirectory);
            string logLine = BuildLogLine(currentDateTime, logMessage);
            LogDirectory = (LogDirectory + "Log_" + LogFileName(DateTime.Now) + ".txt");
            lock (typeof(LogWriter))
            {
                StreamWriter logSWriter = null;
                try
                {
                    logSWriter = new StreamWriter(LogDirectory, true);
                    logSWriter.WriteLine(logLine);
                    successStatus = true;
                }
                catch
                {
                }
                finally
                {
                    if (logSWriter != null)
                    {
                        logSWriter.Close();
                    }
                }
            }
            return successStatus;
        }
        private static bool CheckCreateLogDirectory(string logPath)
        {
            bool loggingDirectoryExists = false;
            DirectoryInfo dirInfo = new DirectoryInfo(logPath);
            if (dirInfo.Exists)
            {
                loggingDirectoryExists = true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(logPath);
                    loggingDirectoryExists = true;
                }
                catch
                {
                    // Logging failure
                }
            }
            return loggingDirectoryExists;
        }
        private static string BuildLogLine(DateTime currentDateTime, string logMessage)
        {
            StringBuilder loglineStringBuilder = new StringBuilder();
            loglineStringBuilder.Append(LogFileEntryDateTime(currentDateTime));
            loglineStringBuilder.Append("\t");
            loglineStringBuilder.Append(logMessage);
            return loglineStringBuilder.ToString();
        }
        public static string LogFileEntryDateTime(DateTime currentDateTime)
        {
            return currentDateTime.ToString("dd-MM-yyyy HH:mm:ss");
        }
        private static string LogFileName(DateTime currentDateTime)
        {
            return currentDateTime.ToString("dd_MM_yyyy");

        }
    }
}