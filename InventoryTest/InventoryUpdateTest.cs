﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace InventoryTest
{
    [TestClass]
  
    public class InventoryUpdateTest
    {
        BusinessLogic.Inventory blInventory = new BusinessLogic.Inventory();

        string inputDir = ConfigurationManager.AppSettings["InputFileDirectory"].ToString();
        string outputDir = ConfigurationManager.AppSettings["OutputFileDirectory"].ToString();
        string filename = "StockInput";
        string rightResult = "StockInputResult.txt";

        [TestMethod]
        public void passAllArguments()
        {
           blInventory.UpdateInventories(filename, inputDir, outputDir);
            //check output file created
           string fileAContent = File.ReadAllText(outputDir + filename+ DateTime.Now.ToString("ddMMyyyyHHmm") + ".txt");
           Assert.IsFalse(string.IsNullOrEmpty(fileAContent));           
        }

        [TestMethod]
        public void NoInputDir()
        {
            //should use App config setting
            blInventory.UpdateInventories(filename, "", outputDir);
            //check output file created
            string fileAContent = File.ReadAllText(outputDir + filename + DateTime.Now.ToString("ddMMyyyyHHmm") + ".txt");
            Assert.IsFalse(string.IsNullOrEmpty(fileAContent));
        }

        [TestMethod]
        public void NoOutputDir()
        {
            //should use App config setting
            blInventory.UpdateInventories(filename, inputDir, "");
            //check output file created
            string fileAContent = File.ReadAllText(outputDir + filename + DateTime.Now.ToString("ddMMyyyyHHmm") + ".txt");
            Assert.IsFalse(string.IsNullOrEmpty(fileAContent));
        }

        [TestMethod]
        public void CheckInventoryRules()
        {
            //Check output file match the correct result - the input file has all qality/sellin rules
            blInventory.UpdateInventories(filename, inputDir, outputDir);
            //check output file against file with correct results 
            string inputfileContent = File.ReadAllText(outputDir + filename + DateTime.Now.ToString("ddMMyyyyHHmm") + ".txt");
            string resultfileAContent = File.ReadAllText(outputDir + rightResult);
            Assert.AreEqual(inputfileContent, resultfileAContent);
        }

        [TestMethod]
        public void NoInputFile()
        {
            //test no input file passed
            try
            { blInventory.UpdateInventories("", inputDir, outputDir); }
            catch( Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            blInventory = null;
        }
    }
}
