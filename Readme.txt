Inventory Management App
========================

A standalone program to update inventory data in text file with specified rules and generate output text file.

Getting Started
================
Get program codes from Git by clicking Download and copy below URL repository:
https://saharahmed2016@bitbucket.org/saharahmed2016/inventory-management-app.git


Build the application: 
=======================
 .Start Visual Studio
 .On the menu bar, choose file, open project
 .Select the directory where codes downloaded
 .Build the project using build option

Run the Application:
====================
 .When build the project the executable(exe) file will be generated in 'Inventory Management App/ConsoleInventoryApplication/bin'
 .click on any folder on desktop, Press Win+R and write cmd.exe to run Command prompt
 .Enter full path and name of the exe file surrounded by quote
 .Then add input file name without extension surrounded by quote
 .Also add input file path end with '\' extension surrounded by quote
 .Also add output file path end with '\' extension surrounded by quote

For Example: 

"C:\Document\ConsoleInventoryApplication.exe" "StockInput" "c:\Temp\" "c:\Temp\output\"

The Application output:
=======================
.The output file will not be generated and error will be logged in the 'Log_TodayDate', if input file name is null or not passed as parameter
.If input/output path not supplied the application will use AppSetting values
.Application will create Log file in path that specified in the AppSetting. 
.There will be log file per a day with name format as 'Log_dd_mm_yyy.txt' 
.Application will amend log file when process failed to generate the output file. 
.Application will add message if output file successfully generated
 

Test the Application:
=====================
InventoryTest nunit project added to the application to test the following:
.Check the quality&Sellin values per item updated correctly per rule, by comparing the output file with correct output file that saved in TestResult/StockInputResult.txt
.Raise error message when input file not passed as argument.
.When input/output path not provided as arguments, the application should use the path in AppSetting
.Run correctly when all arguments passed


