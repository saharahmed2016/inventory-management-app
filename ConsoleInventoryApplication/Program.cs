﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;
using SharedDll;

namespace Inventory
{
    class Program
    {
        static void Main(string[] args)
        {
            LogWriter oErrorLog = new LogWriter();
            BusinessLogic.Inventory beInventory = new BusinessLogic.Inventory();

            string fileName = string.Empty;
            string inputFolder = string.Empty;
            string outputFolder = string.Empty;
            try { 
                //Get argument values
                if (args.Count() > 0)   //Check if input file name  provided
                { 
                    fileName = args[0];      //File name without extention and Cant be null
                    if (!string.IsNullOrEmpty(fileName.Trim()))
                    {
                        if (args.Count() > 1)   //Check if input path provided
                            inputFolder = args[1];
                        if (args.Count() > 2)   //Check if output path provided
                            outputFolder = args[2]; 

                        //Update Input Inventories data and create output file
                        beInventory.UpdateInventories(fileName, inputFolder, outputFolder);
                    }
                    else
                    {
                        oErrorLog.WriteErrorLog("File Name cant be null");
                    }
                } else
                {
                    oErrorLog.WriteErrorLog("File Name not provided");
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
            finally
            {
                oErrorLog = null;
                beInventory = null;
            }

        }
    }
}
