﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using BusinessEntities;
using SharedDll;

namespace BusinessLogic
{
    public class Inventory : Base
    {
        public Inventory()
            : base()
        {
            //can be use to pass connection string for example
            base.New();
        }

        #region "Public Methods"
        /// <summary>
        /// Update the quality and Sellin days for each items in input file.
        /// Save the changes as text file into output path.
        /// </summary>
        /// <param name="fileName">file name without extension</param>
        /// <param name="inputFilePath">path of input file</param>
        /// <param name="outputFilePath">path of output file</param>
        public void UpdateInventories(string fileName, string inputFilePath, string outputFilePath)
        {
            LogWriter oLogWriter = new LogWriter();
            string fullFileName = "";  // File name with path
            List<BusinessEntities.Stock> beStocks = new List<BusinessEntities.Stock>();
             
            try
            {
                string logMssge = string.Empty;
                if (!String.IsNullOrEmpty(fileName))
                {
                    if (string.IsNullOrEmpty(inputFilePath.Trim()))
                    {
                        //Get Default input file path from App Config
                       inputFilePath = ConfigurationManager.AppSettings["InputFileDirectory"];
                    }

                     if (string.IsNullOrEmpty(outputFilePath.Trim()))
                    {
                       // Get default output file path from App Config
                        outputFilePath = ConfigurationManager.AppSettings["OutputFileDirectory"].ToString();
                    }

                    fullFileName = inputFilePath + fileName + ".txt";
  
                    if (File.Exists(fullFileName))
                    {
                        //Read inventory data from input file
                        logMssge = readInventoryInputFile(fullFileName, out beStocks);

                        if (logMssge == string.Empty)
                        {
                            //Apply rule to input data
                            logMssge = AmendInventoryInputData(ref beStocks);
                            if (logMssge == string.Empty)
                            {
                                 logMssge = saveOutputData( fileName, outputFilePath, beStocks);
                                 oLogWriter.WriteErrorLog(logMssge);
                                                           
                            }  else {
                                oLogWriter.WriteErrorLog(logMssge);
                            }                   

                        }  else {
                            oLogWriter.WriteErrorLog(logMssge);
                        }   
                    } else
                    {
                        oLogWriter.WriteErrorLog(string.Format("{0} File doesnt  exists", fullFileName));
                    }
                } else
                {
                    oLogWriter.WriteErrorLog("Input File Name not provided");
                }
            }
            catch (Exception ex)
            {
                oLogWriter.WriteErrorLog(ex.ToString());
            }
            finally
            {
                oLogWriter = null;
                beStocks = null;
            }

        }

        #endregion "Public Methods"
        
        #region "Private Methods"

        /// <summary>
        /// Read Inventory input data save it in business data classes
        /// </summary>
        /// <param name="inventoryInputFullFileName">Input file name and path</param>
        /// <param name="beStocks">An array of input stockes data </param>
        /// <returns>Error message to be saved in log file. When null no error</returns>
        private string readInventoryInputFile(string inventoryInputFullFileName, out List<BusinessEntities.Stock> beStocks )
        {
            beStocks = new List<BusinessEntities.Stock>();
            string inputErrorMessage = "";
  
            try 
	        {
                // Read input file
	            using (StreamReader srInputData = new StreamReader(inventoryInputFullFileName))
                {
                    //Check if there is data in input file
                    if (srInputData.Peek() != -1)
                    {
                        while (true)
                        {
                            string line = srInputData.ReadLine();
                            if (line == null)
                            {
                                //end of file
                                break;
                            } else
                            {
                                BusinessEntities.Stock beStock = new BusinessEntities.Stock();

                                //Get Quality value
                                string qualityStr = line.Substring(line.LastIndexOf(' ')+1) ;
                                    
                                //Get Item name and SellIn without Quality and space
                                string lineWithoutQuality = line.Substring(0, line.Length - qualityStr.Length - 1);

                                //Get SellIn value
                                string sellInStr = lineWithoutQuality.Substring(lineWithoutQuality.LastIndexOf(' ') + 1);
                                    
                                //Get Item name
                                string itemName = lineWithoutQuality.Substring(0, lineWithoutQuality.Length - sellInStr.Length - 1);

                                beStock.Quality = System.Convert.ToInt16(qualityStr);
                                beStock.SellinDays = System.Convert.ToInt16(sellInStr);
                                beStock.Name = itemName;

                                beStocks.Add(beStock);
                            }    
                        }
                    } else
                    {
                        // No data found
                        inputErrorMessage = "File has no data!  - " + inventoryInputFullFileName;
                    }
                }
	        }
	        catch (Exception ex)
	        {
                   inputErrorMessage = ex.ToString();
	        }
            finally
            {
                
            }

            return inputErrorMessage;
        }

        /// <summary>
        /// Apply all business rules to input data
        /// </summary>
        /// <param name="beStocks">An array of stockes data </param>
        /// <returns>Error message to be saveed in log file if null no error</returns>
        private string AmendInventoryInputData(ref List<BusinessEntities.Stock> beStocks)
        {
            string outputErrorMessage = "";
            string itemNameLowerCase = "";
            try 
	        {
                foreach (BusinessEntities.Stock stock in beStocks)
                {
                    itemNameLowerCase = stock.Name.ToLower();

                    switch (itemNameLowerCase)
                    {
                        case ItemNames.AgedBrie:
                            if (stock.SellinDays < 0)
                            {
                                stock.Quality = stock.Quality - 2;
                            }else
                            {
                                stock.Quality += stock.Quality;
                            }
                            break;
                        case ItemNames.BackstagePasses:
                            if (stock.SellinDays < 0)
                            {
                                stock.Quality = 0;
                            }
                            else if (stock.SellinDays <= 5)
                            {
                                stock.Quality = stock.Quality + 3;
                            }
                            else if (stock.SellinDays <= 10)
                            {
                                stock.Quality = stock.Quality + 2;
                            }
                            else
                            {
                                stock.Quality += stock.Quality;
                            }
                                break;
                        case ItemNames.Conjured:
                                if (stock.SellinDays < 0)
                                {
                                    stock.Quality = stock.Quality - 4;
                                }
                                else
                                {
                                    stock.Quality = stock.Quality - 2;
                                }
                                break;
                        case ItemNames.NormalItem:
                                if (stock.SellinDays < 0)
                                {
                                    stock.Quality = stock.Quality - 2;
                                }
                                else
                                {
                                    stock.Quality = stock.Quality - 1;
                                }
                                break;
                        case ItemNames.Sulfuras:
                            // unchanged as legendary item
                                break;
                        default:
                                stock.Name = "NO SUCH ITEM";
                                stock.Quality = null;
                                stock.SellinDays = null;
                            break;
                    }

                    //Apply quality rule - quality value never more than 50 or negative
                    stock.Quality = ApplyQualityLimitRules(stock.Quality);

                    if (itemNameLowerCase != ItemNames.Sulfuras)
                    {
                        //Lower sellin value by 1 except for Sulfuras never change
                        stock.SellinDays = ApplySellInRules(stock.SellinDays);
                    }
                }         
            }
             catch (Exception ex)
	        {
                   outputErrorMessage = ex.ToString();
	        }
            finally
            {
                
            }
            return outputErrorMessage;
        }

        /// <summary>
        /// Saved updated stock details into specified directory
        /// </summary>
        /// <param name="inputFileName"> input file name</param>
        /// <param name="outputFilePath">path where to save the file</param>
        /// <param name="beStocks">An array of updated stockes data </param>
        /// <returns>Log details - error or sucessfully saved message to be logged in the log file</returns>         
        private string saveOutputData(string inputFileName, string outputFilePath, List<BusinessEntities.Stock> beStocks)
        {
            string saveLogMessage = "";
            string fullOutputFileName  = "";

            try
            {
                //Save changes into output file
                bool outputFilePathExists = Directory.Exists(outputFilePath);
                if (!outputFilePathExists)
                { Directory.CreateDirectory(outputFilePath); }

                fullOutputFileName = outputFilePath  + inputFileName + DateTime.Now.ToString("ddMMyyyyHHmm") + ".txt";
                using (StreamWriter swOutputData = new StreamWriter(fullOutputFileName))
                {
                    foreach (BusinessEntities.Stock itemDetails in beStocks)
                    {
                        swOutputData.WriteLine(itemDetails.Name + " " + itemDetails.SellinDays.ToString() + " " + itemDetails.Quality.ToString());
                    }
                }

                //output file sucessfully saved
                saveLogMessage = string.Format("{0} output file created", fullOutputFileName);
            }
            catch (Exception ex)
            {
                saveLogMessage = ex.ToString();
            }
            finally
            {
                
            }
            return saveLogMessage;
        }
        
        /// <summary>
        /// Apply Sellin rules- Always lower sellin value 
        /// </summary>
        /// <param name="oldSellIn">/Old Stock Sellin</param>
        /// <returns>updated sell in value</returns>
        private int? ApplySellInRules(int? oldSellIn)
        {                            
            //Always lower sellin value
            int? nSellin = oldSellIn - 1;
            
            return nSellin;
        }


        /// <summary>
        /// Apply quality rule - quality value never more than 50 or negative
        /// </summary>
        /// <param name="stockQuality">Quality value</param>
        /// <returns>updated quality value</returns>
        private int? ApplyQualityLimitRules(int? stockQuality)
        {
            int? updatedQuality = stockQuality;
              if (stockQuality < 0)
              {
                  updatedQuality = 0; 
              }  else if (stockQuality > 50)
                  updatedQuality = 50;

              return updatedQuality;
        }
        #endregion "Private Methods"

    }
}
